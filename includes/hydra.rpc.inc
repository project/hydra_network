<?php

/**
 * @file
 * Handling of XMLRPC calls for the Hydra environment.
 * @ingroup hydra
 */

function hydra_user_can_share($site, $node, $account = NULL) {
  if (!$account) {
    global $user;
    if ($user->uid == 0) {
      # Se l'utente non è loggato ASSUMIAMO che l'operazione sia stata
      # richiesta da uno script ed utilizziamo l'utente del nodo
      if (!($account = user_load(array('uid' => $node->uid)))) {
        return FALSE;
      }
    }
    else {
      $account = $user;
    }
  }
  switch ($node->type) {
    case 'blog':
      $perm = 'create blog entries';
      break;

    default:
      $perm = "create {$node->type} content";
      break;
  }
  return xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_user_access', $account->uid, $perm);
}

function hydra_get_remote_site_name($site) {
  global $_hydra_current_site;
  if ($site->sid == $_hydra_current_site->sid) {
    return variable_get('site_name', "Hydra site #{$site->sid}");
  }
  static $cached_site_name;

  if ($cached_site_name) {
    if (isset($cached_site_name[$site->sid])) {
      return $cached_site_name[$site->sid];
    }
  }
  $site_name = hydra_remote_variable_get($site, 'site_name', "Hydra site #{$site->sid}");
  $cached_site_name[$site->sid] = $site_name;
  return $site_name;
}

function hydra_remote_module_exists($site, $module) {
  if (!_hydra_site_available($site)) {
    watchdog('hydra', 'XMLRPC request for module existence made to unavailable site', array('site' => $site->hostname));
    return FALSE;
  }
  return xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_module_exists', $module);
}

function hydra_remote_variable_set($site, $var, $value) {
  if (!_hydra_site_available($site)) {
    watchdog('hydra', 'XMLRPC request for variable set made to unavailable site', array('site' => $site->hostname));
    return $value;
  }
  return xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_variable_set', $var, serialize($value));
}

// We directly query the db for performance reasons
function hydra_remote_variable_get($site, $var, $value) {
  if ($result = db_result(db_query("SELECT value FROM {$site->name}_variable WHERE name = '%s'", $var))) {
    return unserialize($result);
  }
  else {
    return $value;
  }
}

function hydra_remote_node_type_exists($site, $node_type_name) {
  if (!_hydra_site_available($site)) {
    watchdog('hydra', 'XMLRPC request for node type existence made to unavailable site', array('site' => $site->hostname));
    return FALSE;
  }
  return xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_node_type_exists', $node_type_name);
}

function hydra_remote_set_node_type($site, $op, $node_type) {
  if (!_hydra_site_available($site)) {
    watchdog('hydra', "XMLRPC request for node type !op made to unavailable site", array('!op' => $op, 'site' => $site->hostname));
    return FALSE;
  }
  return xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_set_node_type', $op, $node_type);
}

function hydra_remote_create_format($site, $format) {
  if (!_hydra_site_available($site)) {
    watchdog('hydra', "XMLRPC request for format creation made to unavailable site", array('site' => $site->hostname));
    return FALSE;
  }
  return xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_create_format', $format);
}

function hydra_remote_node_invoke_nodeapi($site, $op, $nid) {
  if (!_hydra_site_available($site)) {
    watchdog('hydra', 'XMLRPC request for node invoke nodeapi made to unavailable site', array('site' => $site->hostname));
    return FALSE;
  }
  return unserialize(xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_node_invoke_nodeapi', $op, $nid));
}

function hydra_remote_set_content_field_instance($site, $op, $field) {
  if (!_hydra_site_available($site)) {
    watchdog('hydra', "XMLRPC request for content field instance !op made to unavailable site", array('!op' => $op, 'site' => $site->hostname));
    return FALSE;
  }
  return xmlrpc("http://{$site->hostname}/xmlrpc.php", 'hydra.rpc_set_content_field_instance', $op, $field);
}

function hydra_xmlrpc() {
  // TODO: security checks
  return array(
    array(
      'hydra.rpc_user_access',
      'hydra_rpc_user_access',
      array('boolean', 'string', 'string'),
      t('Handling user access request')
    ),
    array(
      'hydra.rpc_module_exists',
      'hydra_rpc_module_exists',
      array('boolean', 'string'),
      t('Handling module exists request')
    ),
    array(
      'hydra.rpc_variable_set',
      'hydra_rpc_variable_set',
      array('boolean', 'string', 'string'),
      t('Handling variable set request')
    ),
    array(
      'hydra.rpc_variable_get',
      'hydra_rpc_variable_get',
      array('string', 'string', 'string'),
      t('Handling variable get request')
    ),
    array(
      'hydra.rpc_node_type_exists',
      'hydra_rpc_node_type_exists',
      array('boolean', 'string'),
      t('Handling check for node type existance request')
    ),
    array(
      'hydra.rpc_set_node_type',
      'hydra_rpc_set_node_type',
      array('boolean', 'string', 'struct'),
      t('Handling of node type creating/updating/deleting request')
    ),
    array(
      'hydra.rpc_create_format',
      'hydra_rpc_create_format',
      array('boolean', 'struct'),
      t('Handling input filters creating request')
    ),
    array(
      'hydra.rpc_node_invoke_nodeapi',
      'hydra_rpc_node_invoke_nodeapi',
      array('string', 'string', 'string'),
      t('Handling check for invoking nodeapi request')
    ),
    array(
      'hydra.rpc_set_content_field_instance',
      'hydra_rpc_set_content_field_instance',
      array('boolean', 'string', 'struct'),
      t('Handling of content field instance creating/updating/deleting request')
    ),
  );
}

function _hydra_site_available($site) {
  return file_exists(getcwd() . "/sites/{$site->hostname}");
}

function hydra_rpc_user_access($uid, $perm) {
  $account = user_load(array('uid' => $uid));
  return user_access($perm, $account);
}

function hydra_rpc_module_exists($module) {
  return module_exists($module);
}

function hydra_rpc_variable_set($var, $value) {
  variable_set($var, unserialize($value));
  return TRUE;
}

function hydra_rpc_variable_get($var, $value) {
  return variable_get($var, unserialize($value));
}

function hydra_rpc_node_type_exists($node_type_name) {
  return (bool)node_get_types('type', $node_type_name);
}

function hydra_rpc_set_node_type($op, $node_type) {
  if ($op == 'insert') {
    // If node type is created with CCK, just save it
    if ($node_type['module'] == 'node' || !$node_type['module']) {
      $type = new stdClass;
      $type->type = $node_type['type'];
      $type->name = $node_type['name'];
      $type->orig_type = $node_type['orig_type'];
      $type->old_type = isset($node_type['old_type']) ? $node_type['old_type'] : $type->type;
      $type->description = $node_type['description'];
      $type->help = $node_type['help'];
      $type->min_word_count = $node_type['min_word_count'];
      $type->title_label = $node_type['title_label'];
      $type->body_label = $node_type['body_label'];
      // title_label is required in core; has_title will always be true, unless a
      // module alters the title field.
      $type->has_title = ($type->title_label != '');
      $type->has_body = ($type->body_label != '');
      $type->module = !empty($node_type['module']) ? $node_type['module'] : 'node';
      $type->custom = FALSE; // This prevents showing 'delete' link in content types overview page
      $type->modified = TRUE;
      $type->locked = TRUE;
      node_type_save($type);
    }
    // Else enable the whole module
    else {
      module_rebuild_cache();
      // Check for module presence (installed and enabled)
      if (!module_exists($node_type['module'])) {
        // If module is not active, enable it
        module_enable(array($node_type['module']));
      }
    }
  }
  elseif ($op == 'update') {
    $type = node_get_types('type', $node_type['old_type']);
    $type->type = $node_type['type'];
    $type->name = $node_type['name'];
    node_type_save($type);
  }
  elseif ($op == 'delete') {
    // If node type is created with CCK, just delete it (don't use node_type_delete because it
    // deletes also CCK fields)
    if ($node_type['module'] == 'node' || !$node_type['module']) {
      db_query("DELETE FROM {node_type} WHERE type = '%s'", $node_type['type']);
    }
    // Else disable the whole module
    else {
      module_disable(array($node_type['module']));
    }
  }
  elseif ($op == 'delete_sql') {
    // Use this $op if you want to delete only node type
    module_disable(array($node_type['module']));
    //db_query("DELETE FROM {node_type} WHERE type = '%s'", $node_type['type']);
  }
  // WARNING: this could lead to timeouts if done on several sites
  node_types_rebuild();
  menu_rebuild();
  return TRUE;
}

function hydra_rpc_create_format($values) {
  $format = isset($values['format']) ? $values['format'] : NULL;
  $current = filter_list_format($format);
  $name = trim($values['name']);
  $cache = TRUE;

  // Add a new filter format.
  if (!$format) {
    $new = TRUE;
    db_query("INSERT INTO {filter_formats} (name) VALUES ('%s')", $name);
    $format = db_result(db_query("SELECT MAX(format) AS format FROM {filter_formats}"));
    drupal_set_message(t('Added input format %format.', array('%format' => $name)));
  }
  else {
    drupal_set_message(t('The input format settings have been updated.'));
  }
  // We store the roles as a string for ease of use.
  // We should always set all roles to TRUE when saving a default role.
  // We use leading and trailing comma's to allow easy substring matching.
  $roles = array();
  if (isset($values['roles'])) {
    foreach ($values['roles'] as $id => $checked) {
      if ($checked) {
        $roles[] = $id;
      }
    }
  }
  if (!empty($values['default_format'])) {
    $roles = ',' . implode(',', array_keys(user_roles())) . ',';
  }
  else {
    $roles = ',' . implode(',', $roles) . ',';
  }

  db_query("UPDATE {filter_formats} SET cache = %d, name='%s', roles = '%s' WHERE format = %d", $cache, $name, $roles, $format);

  cache_clear_all($format . ':', 'cache_filter', TRUE);
  return TRUE;
}

function hydra_rpc_node_invoke_nodeapi($op, $nid) {
  $node = node_load($nid);
  return serialize(node_invoke_nodeapi($node, $op));
}

function hydra_rpc_set_content_field_instance($op, $field) {
  module_load_include('inc', 'content', 'includes/content.crud');
  switch ($op) {
    case 'create instance':
      content_field_instance_create($field);
      break;
    case 'update instance':
      content_field_instance_update($field);
      break;
    case 'delete instance':
      content_field_instance_delete($field['field_name'], $field['type_name']);
      break;
  }
}

<?php

/**
 * @file
 * API functions for the Hydra environment.
 * @ingroup hydra
 */

/**
 * Hydra site's loader function.
 */
function hydra_site_load($sid) {
  if (!is_numeric($sid)) {
    return FALSE;
  }
  static $cached_site;
  if ($cached_site) {
    if (isset($cached_site[$sid])) {
      return $cached_site[$sid];
    }
  }
  $result = db_query("SELECT * FROM {hydra_sites} WHERE sid = %d", $sid);
  if ($site = db_fetch_object($result)) {
    $site->fullname = hydra_get_remote_site_name($site);
    $cached_site[$sid] = $site;
    return $site;
  }
  return FALSE;
}

/**
 * Check Hydra administrator grants.
 *
 * Check if given (or current if none provided) user is an Hydra administrator,
 * that is an user with role "administrator" on the master site.
 */
function hydra_admin_access($account = NULL) {
  global $_hydra_master_site;
  if (!$account) {
    global $user;
    $account = $user;
  }
  if ($account->uid == 1) {
    return TRUE;
  }
  return (bool)db_result(db_query("SELECT uid FROM %s WHERE uid = %d", "{$_hydra_master_site->name}_users_roles", $account->uid));
}

/**
 * Check master site grants.
 *
 * Check if given (or current if none provided) user is an Hydra administrator
 * and the current site is master or template.
 * This is used to protect features that can be accessed only on master or
 * template site.
 */
function hydra_master_access($account = NULL) {
  global $_hydra_current_site;
  if (!$account) {
    global $user;
    $account = $user;
  }
  return (hydra_admin_access($account) && ($_hydra_current_site->master || $_hydra_current_site->template));
}

/**
 * Check node grants.
 *
 * With Hydra it's necessary to check if a node is shared before it can be
 * deleted or modified.
 */
function hydra_node_access($op, $node, $account = NULL) {
  global $_hydra_current_site;
  if (!$account) {
    global $user;
    $account = $user;
  }
  switch ($op) {
    case 'share':
      if (!$_hydra_current_site->shared_nodes) {
        return FALSE;
      }
      // Nodes can be shared only by those with permission, admin or owner
      if (user_access('share all contents on hydra sites', $account) || $account->uid == 1) {
        return TRUE;
      }
      if (user_access('share own contents on hydra sites', $account) && $account->uid == $node->uid) {
        return TRUE;
      }
      break;
    case 'delete':
      // Nodes can be deleted only if:
      // - $user is Hydra admin
      // - $user is owner
      // - $user is site admin and $node has been created on current site
      if (!empty($node->nid) &&
          node_access('delete', $node) &&
          ($node->uid == $account->uid || hydra_admin_access() || ($node->hydra_master_sid == $_hydra_current_site->sid && in_array("administrator", $account->roles)))) {
        return TRUE;
      }
      break;
    case 'update':
      // Nodes can be updated only if:
      // - $user is Hydra admin
      // - $user is owner
      // - $user is site admin and $node has been created on current site
      if (!empty($node->nid) &&
          node_access('update', $node) &&
          ($node->uid == $account->uid || hydra_admin_access() || ($node->hydra_master_sid == $_hydra_current_site->sid && in_array("administrator", $account->roles)))) {
        return TRUE;
      }
      break;
  }
  return FALSE;
}

/**
 * Get a list of db tables by prefix.
 */
function hydra_get_db_tables($prefix) {
  global $db_type;
  if ($db_type == 'pgsql') {
    $query = "SELECT table_name
                FROM information_schema.tables
               WHERE table_schema = 'public'
                 AND table_name LIKE '{$prefix}_%'";
  }
  else {
    $query = "SHOW TABLES LIKE '{$prefix}_%'";
  }
  $result = db_query($query);
  $tables = array();
  while ($table = db_fetch_array($result)) {
    $value = array_values($table);
    $name = str_replace("{$prefix}_", '', $value[0]);
    $tables[$name] = $name;
  }
  return $tables;
}

/**
 * Get a list of the network's sites.
 */
function hydra_get_sites($options = NULL) {
  static $cached_sites;
  $cached_key = $options ? $options['uid'] : '__any__';
  if ($cached_sites) {
    if (isset($cached_sites[$cached_key])) {
      return $cached_sites[$cached_key];
    }
  }
  $join = '';
  if (isset($options['uid'])) {
    $join .= " JOIN {hydra_users} us ON us.sid=s.sid AND us.uid = {$options['uid']}";
  }
  $result = db_query("SELECT *
                        FROM {hydra_sites} s
                        {$join}
                       ORDER BY s.name ASC");
  $sites = array();
  while ($site = db_fetch_object($result)) {
    $site->fullname = hydra_get_remote_site_name($site);
    $sites[] = $site;
  }
  $cached_sites[$cached_key] = $sites;
  return $sites;
}

/**
 * Return the current hostname.
 */
function hydra_get_current_hostname() {
  // Cribbed from bootstrap.inc -- removes port protocols from the host value.
  $hostname = drupal_strtolower(implode('.', array_reverse(explode(':', rtrim($_SERVER['HTTP_HOST'], '.')))));
  return($hostname);
}

/**
 * Check if given site is the current one.
 */
function hydra_is_current_site($site) {
  $hostname = hydra_get_current_hostname();
  $site_hostname = db_result(db_query("SELECT hostname FROM {hydra_sites} WHERE sid = %d", $site->sid));
  return ($hostname == $site_hostname);
}

/**
 * Get the current site object based on the hostname.
 */
function hydra_get_current_site() {
  static $cached_site;
  if ($cached_site) {
    return $cached_site;
  }
  $hostname = hydra_get_current_hostname();
  $site = db_fetch_object(db_query_range("SELECT * FROM {hydra_sites} WHERE hostname = '%s'", $hostname, 0, 1));
  $site->fullname = variable_get('site_name', "Hydra site #{$site->sid}");
  $cached_site = $site;
  return $site;
}

/**
 * Load the template site.
 */
function hydra_get_template_site() {
  static $cached_site = '';
  if ($cached_site) {
    return $cached_site;
  }
  $sid = db_result(db_query_range("SELECT sid FROM {hydra_sites} WHERE template = 1", NULL, 0, 1));
  $cached_site = hydra_site_load($sid);
  return $cached_site;
}

/**
 * Load the master site.
 */
function hydra_get_master_site() {
  static $cached_site = '';
  if ($cached_site) {
    return $cached_site;
  }
  global $_hydra_current_site;
  $site = db_fetch_object(db_query_range("SELECT * FROM {hydra_sites} WHERE master = 1", NULL, 0, 1));
  if ($site->sid == $_hydra_current_site->sid) {
    $site->fullname = variable_get('site_name', "Hydra site #{$site->sid}");
  }
  else {
    $site->fullname = hydra_get_remote_site_name($site);
  }
  $cached_site = $site;
  return $site;
}

function hydra_user_load(&$user) {
  $user->hydra_master_sid = db_result(db_query("SELECT sid
                                                  FROM {hydra_users}
                                                 WHERE uid = %d
                                                   AND sid_master = 1", $user->uid));
}

function hydra_user_join($site, $account, $master = 0) {
  hydra_user_leave($site, $account);
  db_query("INSERT INTO {hydra_users}
            VALUES (%d, %d, %d, %d)", $site->sid, $account->uid, time(), $master);
  watchdog('hydra join', "User {$account->name} joined {$site->fullname}");
}

function hydra_user_leave($site, $account) {
  db_query("DELETE FROM {hydra_users}
             WHERE sid = %d
               AND uid = %d", $site->sid, $account->uid);
  watchdog('hydra left', "User {$account->name} left {$site->fullname}");
}

function hydra_user_has_joined($site, $account = NULL) {
  static $statuses = array();

  if (!$account) {
    global $user;
    $account = $user;
  }
  if (isset($statuses[$site->sid][$account->uid])) {
    return $statuses[$site->sid][$account->uid];
  }
  $status = (bool)db_result(db_query("SELECT uid
                                        FROM {hydra_users}
                                       WHERE sid = %d
                                         AND uid = %d", $site->sid, $account->uid));

  $statuses[$site->sid][$account->uid] = $status;
  return $status;
}

function hydra_node_share($site, $node, $master = 0, $account = NULL) {
  global $_hydra_current_site;
  if ($_hydra_current_site->sid != $site->sid){
    if (!hydra_remote_module_exists($site, node_get_types('module', $node)) || !hydra_remote_node_type_exists($site, $node->type)) {
      hydra_set_error('hydra_node_share', t("The site doesn't support this node type"));
      watchdog('hydra', "!title has not been shared on !site because it doesn't support the node type", array('!title' => check_plain($node->title), '!site' => check_plain($site->fullname)), WATCHDOG_WARNING);
      return FALSE;
    }
    elseif (!hydra_user_can_share($site, $node, $account)) {
      hydra_set_error('hydra_node_share',t("User is not allowed to share on this site"));
      watchdog('hydra', "!title has not been shared on !site because the user is not registered to it", array('!title' => check_plain($node->title), '!site' => check_plain($site->fullname)), WATCHDOG_WARNING);
      return FALSE;
    }
  }
  hydra_node_unshare($site, $node);
  _hydra_node_share($site, $node, $master);
  return TRUE;
}

function _hydra_node_share($site, $node, $master = 0) {
  db_query("INSERT INTO {hydra_nodes} (sid, nid, promote, sticky, shared_at, sid_master)
            VALUES (%d, %d, %d, %d, %d, %d)", $site->sid, $node->nid, $node->hydra_promote, $node->hydra_sticky, time(), $master);
  if ($node->tnid) {
    $translations = db_query("SELECT nid FROM {node} WHERE tnid = %d AND nid <> %d", $node->tnid, $node->nid);
    while ($translation = db_fetch_object($translations)) {
      db_query("INSERT INTO {hydra_nodes} (sid, nid, promote, sticky, shared_at, sid_master)
                VALUES (%d, %d, %d, %d, %d, %d)", $site->sid, $translation->nid, $node->hydra_promote, $node->hydra_sticky, time(), $master);
    }
  }
  watchdog('hydra share', "!title shared on !site", array('!title' => check_plain($node->title), '!site' => check_plain($site->fullname)));
}

function hydra_node_unshare($site,$node) {
  db_query("DELETE FROM {hydra_nodes}
             WHERE sid = %d AND nid = %d", $site->sid, $node->nid);
  if ($node->tnid) {
    $translations = db_query("SELECT nid FROM {node} WHERE tnid = %d AND nid <> %d", $node->tnid, $node->nid);
    while ($translation = db_fetch_object($translations)) {
      db_query("DELETE FROM {hydra_nodes}
                 WHERE sid = %d AND nid = %d", $site->sid, $translation->nid);
    }
  }
  watchdog('hydra unshare', "!title unshared from !site", array('!title' => check_plain($node->title), '!site' => check_plain($site->fullname)));
}

function hydra_node_is_shared($site,$node) {
  return (bool)db_result(db_query("SELECT nid
                                     FROM {hydra_nodes}
                                    WHERE sid = %d
                                      AND nid = %d", $site->sid, $node->nid));
}

function hydra_node_sharing_sites($node) {
  $sites = array();
  $result = db_query("SELECT sid FROM {hydra_nodes} WHERE nid = %d", $node->nid);
  while ($site = db_fetch_object($result)) {
    $sites[] = hydra_site_load($site->sid);
  }
  return $sites;
}

function hydra_voc_share($site, $vid, $master = 0, $global = 0) {
  db_query("INSERT INTO {hydra_vocabularies}
            VALUES (%d, %d, %d, %d)", $site->sid, $vid, $master, $global);
  watchdog('hydra', "Vocabulary with vid !vid has been shared on !site", array('!vid' => $vid, '!site' => check_plain($site->fullname)));
}

function hydra_voc_is_active($site, $vid) {
  return (bool)db_result(db_query("SELECT vid
                                     FROM {hydra_vocabularies}
                                    WHERE sid = %d
                                      AND vid = %d", $site->sid, $vid));
}

function hydra_get_global_vocs() {
  static $cached_vids;

  if ($cached_vids) {
    return $cached_vids;
  }
  $result = db_query("SELECT DISTINCT vid
                        FROM {hydra_vocabularies}
                       WHERE is_global = 1");

  $vids = array();
  while ($voc = db_fetch_object($result)) {
    $vids[] = $voc->vid;
  }

  $cached_vids = $vids;
  return $vids;
}

function hydra_get_local_vocs() {
  $site = hydra_get_current_site();
  $result = db_query('SELECT *
                        FROM {vocabulary} v
                        JOIN {hydra_vocabularies} hv ON hv.vid = v.vid
                       WHERE hv.is_global = 0
                         AND hv.sid = %d
                    ORDER BY v.weight, v.name', $site->sid);


  $vocabularies = array();
  while ($voc = db_fetch_object($result)) {
    $vocabularies[$voc->vid] = $voc;
  }

  return $vocabularies;
}


function hydra_voc_get_master_sid($vid) {
  return db_result(db_query("SELECT sid
                               FROM {hydra_vocabularies}
                              WHERE vid = %d
                                AND sid_master = 1", $vid));
}

function hydra_set_error($name = NULL, $message = '', $reset = FALSE) {
  static $hydra_errors = array();
  if ($reset) {
    $hydra_errors = array();
  }
  if (isset($name) && !isset($hydra_errors[$name])) {
    $hydra_errors[$name] = $message;
  }
  return $hydra_errors;
}

function hydra_get_errors() {
  $hydra_errors = hydra_set_error();
  if (!empty($hydra_errors)) {
    return $hydra_errors;
  }
}

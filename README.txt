
/**
 * @file
 * README file for Hydra.
 * @ingroup hydra
 */ 

 
1.  Introduzione
1.1  La struttura del network
2  Gestione dei siti Hydra
2.1  Creazione di un sito
2.2  Modifica di un sito
2.3  Cancellazione di un sito
3.  Configurazione del sito template
4.  Amministrazione del network
 

----
1.  Introduzione

Hydra è un sistema di gestione di network di siti Drupal che sfrutta il concetto
di "multisite", ovvero una unica installazione del CMS per molti siti: ogni sito
ha una propria directory all'interno della directory "site" di Drupal.
In Hydra le risorse (nodi, utenti e tassonomie) possono, ma non sono
neccessariamente, condivise tra i siti.

----
1.1  La struttura del network

La struttura di un network Hydra è formata da un sito "master", da un sito
"template" e dai siti membri.
Il sito master è quello creato con l'installazione di Drupal ed è responsabile
della creazione degli altri siti e della gestione di determinati aspetti del
network.
Il sito template è quello usato come modello per la creazione degli altri siti,
che di fatto sono suoi cloni. Il sito template può essere definito
dall'amministratore Hydra ed inizialmente è rappresentato dal master.
Considerato che il master è responsabile dell'installazione di determinati moduli
(vedi...), è conveniente che il primo sito creato dal master sia proprio il
template, in modo da garantire che i nuovi siti siano il più possibile puliti.

----
2.  Gestione dei siti Hydra

----
2.1  Creazione di un sito

Prima di creare un sito è neccessario configurare il virtualhost relativo, perché
la creazione prevede una comunicazione dal master verso il nuovo sito tramite
XMLRPC. E' quindi fondamentale che il sito sia raggiungibile.
Una volta che tutto è pronto, è possibile creare un sito tramite l'apposita
interfaccia "admin/hydra/add".

(istruzioni sulla compilazione)



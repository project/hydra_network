<?php

/**
 * @file
 * Page callbacks for the Hydra environment.
 * @ingroup hydra
 */

function hydra_site_form($form_state, $site = NULL) {
  $values = $form_state['values'];
  if ($site) {
    $values = (array)$site;
    $op = $form_state['post']['op'];
    drupal_set_title(t("Modify an Hydra site"));
  }
  else {
    drupal_set_title(t("Create a new Hydra site"));
  }
  if (!$site) {
    $form['name'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Name'),
      '#required'      => TRUE,
      '#default_value' => $values['name'],
      '#description'   => t('The machine-readable site name. This name must contain only lowercase letters, numbers, and underscores. This name must be unique.'),
      '#weight'        => -9
    );
  }
  else {
    $form['item_name'] = array(
      '#type'          => 'item',
      '#title'         => t('Name'),
      '#required'      => TRUE,
      '#value'         => $values['name'],
      '#description'   => t('The machine-readable site name. This name must contain only lowercase letters, numbers, and underscores. This name must be unique.'),
      '#weight'        => -9
    );
    $form['name'] = array(
      '#type'          => 'hidden',
      '#value'         => $values['name'],
    );
  }

  $form['fullname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Full site name'),
    '#required'      => TRUE,
    '#default_value' => $values['fullname'],
    '#description'   => t('The human-readable site name. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#weight'        => -8
  );

  $form['hostname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Hostname'),
    '#required'      => TRUE,
    '#default_value' => $values['hostname'],
    '#description'   => t('Site hostname. Example: <em>www.hydra.com</em>. Must be unique.'),
    '#weight'        => -7
  );

  if (!$site) {
    $form['email'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Site email address'),
      '#default_value' => isset($values['email']) ? $values['email'] : ini_get('sendmail_from'),
      '#maxlength'     => EMAIL_MAX_LENGTH,
      '#description'   => t("The <em>From</em> address in automated emails sent during registration and new password requests, and other notifications. (Use an address ending in your site's domain to help prevent this email being flagged as spam.)"),
      '#required'      => TRUE,
      '#weight'        => -6
    );
    $form['admin'] = array(
      '#type'              => 'textfield',
      '#title'             => t('Admin username'),
      '#maxlength'         => 60,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value'     => $values['admin'],
      '#description'       => t('Pick an existing username as site Administrator. You can leave it empty and create later an user from the new site.'),
      '#weight'            => -5,
    );
  }

  $form['notes'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Notes'),
    '#default_value' => $values['notes'],
    '#rows'          => 10,
    '#description'   => t('Insert here any kind of additional info about this site.'),
    '#weight'        => -4,
  );

  $form['online'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Online'),
    '#default_value' => $values['online'],
    '#description'   => t('Site online status. If unchecked (off-line), the site will appear as <em>under maintenance</em>.'),
    '#weight'        => -3,
  );

  $form['visible'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Visible'),
    '#default_value' => $values['visible'],
    '#description'   => t('Site visibility. If checked, the site will be present in the Hydra bar dropdown menu.'),
    '#weight'        => -2,
  );

  $form['shared_nodes'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Shared nodes'),
    '#default_value' => $values['shared_nodes'],
    '#description'   => t("If checked, this site will share nodes with other network's sites that are sharing nodes."),
    '#weight'        => -1,
  );

  $form['sid'] = array(
    '#type'  => 'hidden',
    '#value' => $values['sid']
  );

  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t('Save')
  );

  if ($site) {
    $form['delete'] = array(
      '#type'   => 'submit',
      '#value'  => t('Delete'),
      '#submit' => array('hydra_site_delete_submit'),
    );
  }

  return $form;
}

function hydra_site_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Delete')) {
    return;
  }
  $template = hydra_get_template_site();

  // Site machine-readable name validations
  $name = trim($form_state['values']['name']);
  if (!isset($form_state['values']['sid'])) {
    if (db_result(db_query("SELECT sid FROM {hydra_sites} WHERE name = '%s'", $name))) {
      form_set_error('type', t('The machine-readable name %type is already taken.', array('%type' => $name)));
    }
  }
  if (!preg_match('!^[a-z0-9_]+$!', $name)) {
    form_set_error('name', t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
  }
  if (is_numeric($name)) {
    form_set_error('name', t('The machine-readable name cannot be a number.'));
  }
  if (is_numeric(drupal_substr($name, 0, 1))) {
    form_set_error('name', t('The machine-readable name cannot begin with a number.'));
  }
  if (in_array($name, array($template->name, 'shared'))) {
    form_set_error('name', t("Invalid machine-readable name. Please enter a name other than %invalid.", array('%invalid' => $name)));
  }

  // Admin validations
  if ($admin = $form_state['values']['admin']) {
    if ($valid_admin = user_validate_name($admin)) {
      form_set_error('admin', $valid_admin);
    }
    elseif (db_result(db_query("SELECT COUNT(*) FROM {users} WHERE LOWER(name) = LOWER('%s')", $admin)) == 0) {
      form_set_error('admin', t('The username %name does not exist.', array('%name' => $admin)));
    }
    elseif (drupal_is_denied('user', $admin)) {
      form_set_error('admin', t('The username %name has been denied access.', array('%name' => $admin)));
    }
  }

  // Site email validations
  if (!isset($form_state['values']['sid'])) {
    if ($valid_email = user_validate_mail($form_state['values']['email'])) {
      form_set_error('email', $valid_email);
    }
  }

  // Site hostname validations
  $hostname = trim($form_state['values']['hostname']);
  // TODO: hostname format check
  if (!isset($form_state['values']['sid'])) {
    if (db_result(db_query("SELECT sid FROM {hydra_sites} WHERE hostname = '%s'", $hostname))) {
      form_set_error('hostname', t('The hostname %name is already taken.', array('%name' => $hostname)));
    }
  }
}

function hydra_site_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] != t('Delete')) {
    if ($sid = $values['sid']) {
      // Update
      $site = hydra_site_load($sid);
      // Set site maintenance according to Hydra status
      hydra_remote_variable_set($site, 'site_offline', (int)!$values['online']);
      hydra_remote_variable_set($site, 'site_name', $values['fullname']);
      if (db_query("UPDATE {hydra_sites}
                       SET name = '%s', notes = '%s', online = %d, visible = %d, hostname = '%s', shared_nodes = %d
                     WHERE sid = %d", $values['name'], $values['notes'], $values['online'], $values['visible'], $values['hostname'], $values['shared_nodes'], $values['sid'])) {
        drupal_set_message(t('Site !sitename has been updated', array('!sitename' => $values['fullname'])));
      }
      else {
        drupal_set_message(t('Error updating !sitename', array('!sitename' => $values['fullname'])), 'error');
      }
    }
    else {
      // Insert
      if (db_query("INSERT INTO {hydra_sites} (name, notes, online, visible, hostname, created, shared_nodes)
                         VALUES ('%s', '%s', %d, %d, '%s', '%s', %d)", $values['name'], $values['notes'], $values['online'], $values['visible'], $values['hostname'], time(), $values['shared_nodes'])) {
         $sid = db_last_insert_id('hydra_sites', 'sid');
        _hydra_install_tables($values);
        _hydra_setup_dir($values);
        _hydra_setup_site($values, $sid);
        drupal_set_message(t('Site !sitename has been created', array('!sitename' => $values['fullname'])));
      }
      else {
        drupal_set_message(t('Error creating !sitename', array('!sitename' => $values['fullname'])), 'error');
      }
    }
    drupal_goto('admin/hydra');
  }
}

function hydra_site_delete_submit($form, &$form_state) {
  $destination = '';
  if (isset($_REQUEST['destination'])) {
    $destination = drupal_get_destination();
    unset($_REQUEST['destination']);
  }
  // Note: We redirect from user/uid/edit to user/uid/delete to make the tabs disappear.
  $form_state['redirect'] = array("admin/hydra/{$form_state['values']['sid']}/delete", $destination);
}

function hydra_site_confirm_delete(&$form_state, $site) {
  $nodes = db_result(db_query("SELECT count(*) FROM {hydra_nodes} WHERE sid = %d AND sid_master = 1", $site->sid));
  $users = db_result(db_query("SELECT count(*) FROM
                                       (SELECT uid, sid
                                          FROM {hydra_users}
                                         GROUP BY uid
                                        HAVING count(*) = 1) a
                                WHERE sid = %d", $site->sid));
  $vocs = db_result(db_query("SELECT count(*) FROM {hydra_vocabularies} WHERE sid = %d AND sid_master = 1", $site->sid));

  $form['_site'] = array('#type' => 'value', '#value' => $site);
  return confirm_form($form,
    t('Are you sure you want to delete the site %name?', array('%name' => $site->name)),
    "admin/hydra/{$site->sid}/edit",
    '<p>' . t('All tables, variables and informations about this site will be deleted.') . '</p>'.
    '<p>' . t('<strong>Warning:</strong> additionally, %nodes, %users and %vocs will be deleted.', array('%nodes' => format_plural($nodes, '1 node', '@count nodes'), '%users' => format_plural($users, '1 user', '@count users'), '%vocs' => format_plural($vocs, '1 vocabulary', '@count vocabularies'))) . '</p>'.
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'), t('Cancel'));
}

function hydra_site_confirm_delete_submit($form, &$form_state) {
  hydra_site_delete($form_state['values']['_site']);
  drupal_set_message(t('Site %name has been deleted.', array('%name' => $form_state['values']['_site']->name)));

  if (!isset($_REQUEST['destination'])) {
    $form_state['redirect'] = 'admin/hydra';
  }
}

function hydra_settings() {
  $form['hydra_network_bar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Network bar'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['hydra_network_bar']['hydra_network_bar'] = array(
    '#title' => t('Show the network bar?'),
    '#type' => 'radios',
    '#options' => array(t('No'), t('Yes'), t('Only to Hydra administrators')),
    '#default_value' => variable_get('hydra_network_bar', 1),
  );
  $form['hydra_disabled_user_db_rewrite'] = array(
    '#type' => 'fieldset',
    '#title' => t('Disabled user db_rewrite_sql'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $paths = array(
    'user/register',
    'admin/user/user/create',
  );
  $form['hydra_disabled_user_db_rewrite']['hydra_disabled_user_db_rewrite'] = array(
    '#type' => 'textarea',
    '#rows' => 5,
    '#cols' => 40,
    '#default_value' => variable_get('hydra_disabled_user_db_rewrite', join("\n", $paths)),
    '#description' => t('Defines the paths for which hook_db_rewrite_sql() on user tables must be disabled. Enter one path per line.'),
  );
  return system_settings_form($form);
}

function hydra_template_form(&$form_state) {
  $form = $sites = array();
  $hydra_sites = hydra_get_sites();
  $template_sid = 0;
  foreach ($hydra_sites as $site) {
    if ($site->template) {
      $template_sid = $site->sid;
      $template_site = $site;
    }
    $sites[$site->sid] = $site->fullname;
  }
  $form['template'] = array(
    '#type' => 'select',
    '#title' => t('Template site'),
    '#options' => $sites,
    '#default_value' => $template_sid,
    '#ahah' => array(
      'path' => 'admin/hydra/settings/template/js',
      'wrapper' => 'template-tables',
    ),
    '#description' => t('Choose which one of the available sites must be template for new sites.'),
  );
  $form['tables'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database tables'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Determines whether a table should be created or copied from template site when creating a new site.'),
  );
  $form['tables']['modules'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="template-tables">',
    '#suffix' => '</div>',
    _hydra_template_get_options_form($template_site),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * FormsAPI for hydra_template_form.
 */
function hydra_template_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (is_numeric($values['template'])) {
    db_query("UPDATE {hydra_sites} SET template = 0");
    db_query("UPDATE {hydra_sites} SET template = 1 WHERE sid = %d", $values['template']);
  }
  db_query("TRUNCATE TABLE {hydra_template_tables}");
  foreach ($values['tables']['modules'][0] as $module) {
    foreach ($module as $table => $copy) {
      db_query("INSERT INTO {hydra_template_tables} VALUES ('%s', %d)", $table, $copy);
    }
  }
  drupal_set_message(t('Template site updated.'));
}

function hydra_sites_list() {
  $sites = hydra_get_sites();

  $rows = array();
  foreach ($sites as $site) {
    $rows[] = array(
      l($site->name, "admin/hydra/{$site->sid}/edit"),
      $site->fullname,
      l($site->hostname, "http://{$site->hostname}"),
      ($site->online) ? t('Yes') : t('No'),
      ($site->visible) ? t('Yes') : t('No'),
    );
  }
  $header = array(t('Name'), t('Full site name'), t('Hostname'), t('Online'), t('Visible'));
  return theme('table', $header, $rows);
}

function hydra_sites_statistics() {
  $sites = hydra_get_sites();

  $rows = array();
  foreach ($sites as $site) {
    $users = db_result(db_query('SELECT count(*) FROM {users} u
                                   JOIN {hydra_users} hu on u.uid = hu.uid
                                  WHERE hu.sid = %d', $site->sid));
    $access_3_mounths = db_result(db_query('SELECT count(*) from {users} u
                                              JOIN {hydra_users} hu on u.uid = hu.uid
                                             WHERE hu.sid = %d
                                               AND u.access >= unix_timestamp(date_sub(curdate(), interval 3 month))', $site->sid));
    $access_12_mounths = db_result(db_query('SELECT count(*) from {users} u
                                               JOIN {hydra_users} hu on u.uid = hu.uid
                                              WHERE hu.sid = %d
                                                AND u.access >= unix_timestamp(date_sub(curdate(), interval 1 year))', $site->sid));
    $access_hydra_start = db_result(db_query('SELECT count(*) from {users} u
                                                JOIN {hydra_users} hu on u.uid = hu.uid
                                               WHERE hu.sid = %d
                                                 AND u.access >= unix_timestamp("2008-12-23")', $site->sid));
    $other_sites = db_result(db_query('SELECT count(u.uid) from {users} u
                                         JOIN {hydra_users} hu on u.uid = hu.uid
                                         JOIN (SELECT u.uid from {users} u
                                                 JOIN {hydra_users} hu on u.uid = hu.uid
                                                WHERE hu.sid <> %d
                                                GROUP BY u.uid
                                               HAVING count(*)> 1) hus on u.uid = hus.uid
                                         WHERE hu.sid = %d', $site->sid, $site->sid));

    $rows[] = array(
      $site->fullname,
      $users,
      $access_3_mounths,
      $access_12_mounths,
      $access_hydra_start,
      $other_sites
    );
  }
  $header = array(t('Full site name'), t('Users'), t('Access over the last 3 months'), t('Access over the last 12 months'), t('Access synce Hydra starts'), t('Registered in other sites'));
  return theme('table', $header, $rows);
}

function hydra_site_delete($site) {
  // Delete every node created on the site, even if they're shared on other sites
  $result = db_query("SELECT nid FROM {hydra_nodes} WHERE sid = %d AND sid_master = 1", $site->sid);
  while ($node = db_fetch_object($result)) {
    node_delete($node->nid); // This invokes nodeapi delete too
  }
  // Delete every user registered on the site, but only if they haven't joined other sites
  $result = db_query("SELECT uid FROM
                              (SELECT uid, sid
                                 FROM {hydra_users}
                                GROUP BY uid
                               HAVING count(*) = 1) a
                       WHERE sid = %d", $site->sid);
  while ($account = db_fetch_object($result)) {
    user_delete(array(), $account->uid); // This invokes hook_user too
  }
  // Delete every vocabulary created on the site, even if they're shared on other sites
  $result = db_query("SELECT vid FROM {hydra_vocabularies} WHERE sid = %d AND sid_master = 1", $site->sid);
  while ($voc = db_fetch_object($result)) {
    taxonomy_del_vocabulary($voc->vid); // This invokes hook_taxonomy too
  }

  db_query("DELETE FROM {hydra_sites} WHERE sid = %d", $site->sid);

  // Delete Drupal site tables
  if ($result = db_query("SHOW TABLES LIKE '{$site->name}_%'")) {
    while ($table = db_result($result)) {
      @db_query("DROP TABLE {$table}");
    }
  }
  _hydra_delete_dir($site);
}

function hydra_share_form($form_state,$node) {
  drupal_set_title(t("Share on other sites"));

  $values = $form_state['values'];

  $sites = hydra_get_sites();

  $current = NULL;

  $form['hydra_sites']['sites'] = array(
    '#type'        => 'checkboxes',
    '#title'       => t('Available sites'),
    '#description' => t('Select the other sites you want to share this node to.'),
    '#weight'      => 1,
  );
  foreach ($sites as $n => $site) {
    if (!$site->master && !$site->template && $site->shared_nodes) {
      if ($node->hydra_master_sid == $site->sid) {
        $current = $site;
      }
      elseif (hydra_user_has_joined($site)) {
        $disabled = FALSE;
        $warning = '';
        if (!hydra_remote_module_exists($site, node_get_types('module', $node)) || !hydra_remote_node_type_exists($site, $node->type)) {
          $disabled = TRUE;
          $warning = ' - <em>' . t("The site doesn't support this node type") . '</em>';
        }
        elseif (!hydra_user_can_share($site, $node)) {
          $disabled = TRUE;
          $warning = ' - <em>' . t("You are not allowed to share on this site") . '</em>';
        }
        $form['hydra_sites']['sites'][$site->sid] = array(
          '#type'          => 'checkbox',
          '#title'         => $site->fullname . $warning,
          '#default_value' => hydra_node_is_shared($site, $node),
          '#return_value'  => $site->sid,
          '#disabled'      => $disabled,
        );
      }
    }
  }

  if ($current) {
    $form['hydra_sites']['current_site'] = array(
      '#title'    => $current->fullname,
      '#type'     => 'checkbox',
      '#value'    => TRUE,
      '#disabled' => TRUE,
      '#weight'   => 0,
    );
  }

  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}

function hydra_share_form_submit($form, &$form_state) {
  global $_hydra_current_site;
  $values = $form_state['values'];
  $shared = $unshared = array();

  foreach ($values['sites'] as $sid => $checked) {
    $site = hydra_site_load($sid);
    $node = node_load($values['nid']);
    $is_shared = hydra_node_is_shared($site, $node);
    if ($is_shared && !$checked) {
      hydra_node_unshare($site, $node);
      $unshared[] = $site->fullname;
      hydra_remote_node_invoke_nodeapi($site, 'hydra_unshare', $values['nid']);
    }
    elseif (!$is_shared && $checked) {
      _hydra_node_share($site, $node);
      $shared[] = $site->fullname;
      hydra_remote_node_invoke_nodeapi($site, 'hydra_share', $values['nid']);
    }
  }
  if (count($shared) > 0) {
    drupal_set_message(t('This content and its translations have been shared on the following site(s): !list', array('!list' => theme('item_list', $shared))));
  }
  if (count($unshared) > 0) {
    drupal_set_message(t('This content and its translations have been unshared form the following site(s): !list', array('!list' => theme('item_list', $unshared))));
  }
}

function hydra_page_default() {
  global $_hydra_current_site, $language;
  $where = (module_exists('translation')) ? "AND n.language = '{$language->language}'" : '';
  // We assume that {hydra_nodes} is joined using hook_db_rewrite_sql
  $query = db_rewrite_sql('SELECT n.nid
                             FROM {node} n
                            WHERE hn.promote = 1
                              AND n.status = 1 ' . $where . '
                            ORDER BY hn.sticky DESC, n.created DESC');
  $result = pager_query($query, variable_get('default_nodes_main', 10));

  $output = '';
  $num_rows = FALSE;
  while ($node = db_fetch_object($result)) {
    $output .= node_view(node_load($node->nid), 1);
    $num_rows = TRUE;
  }

  if ($num_rows) {
    $feed_url = url('rss.xml', array('absolute' => TRUE));
    drupal_add_feed($feed_url, variable_get('site_name', 'Drupal') . ' ' . t('RSS'));
    $output .= theme('pager', NULL, variable_get('default_nodes_main', 10));
  }
  else {
    $default_message = t('<h1 class="title">Welcome to your new Drupal website!</h1><p>Please follow these steps to set up and start using your website:</p>');
    $default_message .= '<ol>';

    $default_message .= '<li>' . t('<strong>Configure your website</strong> Once logged in, visit the <a href="@admin">administration section</a>, where you can <a href="@config">customize and configure</a> all aspects of your website.', array('@admin' => url('admin'), '@config' => url('admin/settings'))) . '</li>';
    $default_message .= '<li>' . t('<strong>Enable additional functionality</strong> Next, visit the <a href="@modules">module list</a> and enable features which suit your specific needs. You can find additional modules in the <a href="@download_modules">Drupal modules download section</a>.', array('@modules' => url('admin/build/modules'), '@download_modules' => 'http://drupal.org/project/modules')) . '</li>';
    $default_message .= '<li>' . t('<strong>Customize your website design</strong> To change the "look and feel" of your website, visit the <a href="@themes">themes section</a>. You may choose from one of the included themes or download additional themes from the <a href="@download_themes">Drupal themes download section</a>.', array('@themes' => url('admin/build/themes'), '@download_themes' => 'http://drupal.org/project/themes')) . '</li>';
    $default_message .= '<li>' . t('<strong>Start posting content</strong> Finally, you can <a href="@content">create content</a> for your website. This message will disappear once you have promoted a post to the front page.', array('@content' => url('node/add'))) . '</li>';
    $default_message .= '</ol>';
    $default_message .= '<p>'. t('For more information, please refer to the <a href="@help">help section</a>, or the <a href="@handbook">online Drupal handbooks</a>. You may also post at the <a href="@forum">Drupal forum</a>, or view the wide range of <a href="@support">other support options</a> available.', array('@help' => url('admin/help'), '@handbook' => 'http://drupal.org/handbooks', '@forum' => 'http://drupal.org/forum', '@support' => 'http://drupal.org/support')) . '</p>';

    $output = '<div id="first-time">' . $default_message . '</div>';
  }
  drupal_set_title('');

  return $output;
}

function hydra_access_denied() {
  drupal_set_title(t('Access denied'));
  return t('You are not authorized to access this page.');
}

function hydra_not_found() {
  drupal_set_title(t('Page not found'));
  return t('The requested page could not be found.');
}


/**
 * @file
 * Javascript file for the Hydra environment.
 * @ingroup hydra
 */

Drupal.behaviors.addHydraMenu = function() {
  if ($('#hydra-menu').length > 0) {
    $('body').addClass('hydra-menu');
  }
};

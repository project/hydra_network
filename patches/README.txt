
/**
 * @file
 * README file for Hydra patches.
 * @ingroup hydra
 */ 

 
Each patch must be applied from the Drupal root.
They all add db_rewrite_sql support where needed.

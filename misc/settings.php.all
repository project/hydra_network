<?php
// $Id$

/**
 *
 * @file
 * settings.php.template
 *
 * This file must be copied to "sites/all" as "settings.php".
 *
 * @ingroup hydra
 */
 
/**
 * Database settings:
 *
 * Note that the $db_url variable gets parsed using PHP's built-in
 * URL parser (i.e. using the "parse_url()" function) so make sure
 * not to confuse the parser. If your username, password
 * or database name contain characters used to delineate
 * $db_url parts, you can escape them via URI hex encodings:
 *
 *   : = %3a   / = %2f   @ = %40
 *   + = %2b   ( = %28   ) = %29
 *   ? = %3f   = = %3d   & = %26
 *
 * To specify multiple connections to be used in your site (i.e. for
 * complex custom modules) you can also specify an associative array
 * of $db_url variables with the 'default' element used until otherwise
 * requested.
 *
 * Database URL format:
 *   $db_url = 'mysql://username:password@localhost/databasename';
 *   $db_url = 'mysqli://username:password@localhost/databasename';
 *   $db_url = 'pgsql://username:password@localhost/databasename';
 */
 
$db_url = 'mysql://username:password@localhost/databasename';

/**
 * Access control for update.php script
 *
 * If you are updating your Drupal installation using the update.php script
 * being not logged in as administrator, you will need to modify the access
 * check statement below. Change the FALSE to a TRUE to disable the access
 * check. After finishing the upgrade, be sure to open this file again
 * and change the TRUE back to a FALSE!
 */

$update_free_access = FALSE;

/**
 * PHP settings:
 *
 * To see what PHP settings are possible, including whether they can
 * be set at runtime (ie., when ini_set() occurs), read the PHP
 * documentation at http://www.php.net/manual/en/ini.php#ini.list
 * and take a look at the .htaccess file to see which non-runtime
 * settings are used there. Settings defined here should not be
 * duplicated there so as to avoid conflict issues.
 */
 
ini_set('arg_separator.output',     '&amp;');
ini_set('magic_quotes_runtime',     0);
ini_set('magic_quotes_sybase',      0);
ini_set('session.cache_expire',     200000);
ini_set('session.cache_limiter',    'none');
ini_set('session.cookie_lifetime',  2000000);
ini_set('session.gc_maxlifetime',   200000);
ini_set('session.save_handler',     'user');
ini_set('session.use_only_cookies', 1);
ini_set('session.use_trans_sid',    0);
ini_set('url_rewriter.tags',        '');

/**
 * The following routine automatically configures shared tables prefixes for all
 * sites.
 */
if (!function_exists('mysqli_init') && !extension_loaded('mysqli')) {
  die('Unable to use the MySQLi database because the MySQLi extension for PHP is not installed. Check your <code>php.ini</code> to see how you can enable it.');
} else {
  $tmp = array();
  $tmp['url'] = parse_url($db_url);

  // Decode url-encoded information in the db connection string
  $tmp['url']['user'] = urldecode($tmp['url']['user']);
  $tmp['url']['pass'] = isset($tmp['url']['pass']) ? urldecode($tmp['url']['pass']) : '';
  $tmp['url']['host'] = urldecode($tmp['url']['host']);
  $tmp['url']['path'] = urldecode($tmp['url']['path']);
  if (!isset($tmp['url']['port'])) {
    $tmp['url']['port'] = NULL;
  }

  $tmp['connection'] = mysqli_init();
  @mysqli_real_connect($tmp['connection'], $tmp['url']['host'], $tmp['url']['user'], $tmp['url']['pass'], substr($tmp['url']['path'], 1), $tmp['url']['port'], NULL, MYSQLI_CLIENT_FOUND_ROWS);

  if (mysqli_connect_errno() > 0) {
    die(mysqli_connect_error());
  }
  
  $db_prefix = array();
  
  $tmp['rs'] = mysqli_query($tmp['connection'], "SHOW TABLES LIKE 'shared_%'");
  while($tmp['row'] = mysqli_fetch_array($tmp['rs'])) {
    $db_prefix[str_replace('shared_', '', $tmp['row'][0])] = 'shared_'; 
  }
  
  mysqli_close($tmp['connection']);
  
  unset($tmp);
}

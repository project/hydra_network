<?php

/**
 * @file
 * Private functions.
 * @ingroup hydra
 */

/**
 * Callback for massive node operations.
 */
function _hydra_mass_update($nids, $op, $value) {
  global $_hydra_current_site;

  db_query("UPDATE {hydra_nodes} SET {$op} = %d WHERE nid IN (" . db_placeholders($nids, 'int') . ") AND sid = %d", $value, $_hydra_current_site->sid);
}

function _hydra_edit_admin_access($account) {
  if ($account->uid == 1) {
    return hydra_admin_access();
  }
  else {
    return user_edit_access($account);
  }
}

/**
 * Check if  db_rewrite_sql need to be disabled.
 *
 * Some pages require it.
 *
 * @todo
 *   Make this an hook.
 */
function _hydra_can_disable_user_db_rewrite() {
  // Are we logging in?
  if (strstr($_POST['form_id'], 'user_login')) {
    return TRUE;
  }

  $pages = array(
    'user/register',
    'admin/user/user/create',
  );
  $pages = variable_get('hydra_disabled_user_db_rewrite', join("\n", $pages));

  $path = drupal_get_path_alias($_GET['q']);
  // Compare with the internal and path alias (if any).
  $page_match = drupal_match_path($path, $pages);
  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
  }

  if ($page_match) {
    return TRUE;
  }
}

function _hydra_get_modules_protection($values) {
  $modules = array();
  foreach ($values as $name => $value) {
    $modules[$name] = db_result(db_query("SELECT protected FROM {hydra_modules} WHERE name = '%s'",$name));
  }
  return $modules;
}

/**
 * Helper sort function taken from domain_prefix.module
 */
function _hydra_table_sort($a, $b) {
  $_a = str_replace('_', '', $a['tablename']);
  $_b = str_replace('_', '', $b['tablename']);
  if ($a['module'] == $b['module']) {
    return strcmp($_a, $_b);
  }
  return strcmp($a['module'], $b['module']);
}

/**
 * Options section for the template site form.
 */
function _hydra_template_get_options_form($template_site) {
  $form = array();
  // Get database schema
  $schema = drupal_get_schema();
  uasort($schema, '_hydra_table_sort');
  // Get template site's tables directly from db
  $db_tables = hydra_get_db_tables($template_site->name);
  // Get template site's tables configuration
  $template_tables = _hydra_get_template_tables();
  $module = '';
  foreach ($schema as $table => $def) {
    if (isset($db_tables[$table])) {
      if (($def['module'] != $module)) {
        $form[$def['module']] = array(
          '#type' => 'fieldset',
          '#title' => $def['module'],
          '#tree' => TRUE,
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $module = $def['module'];
      }
      $form[$def['module']][$table] = array(
        '#type' => 'radios',
        '#title' => $table,
        '#options' => array(
          t('Create'),
          t('Copy')
        ),
        '#default_value' => isset($template_tables[$table]) ? $template_tables[$table] : 0,
      );
    }
  }
  return $form;
}

/**
 * Callback for ajax refresh of template site options.
 */
function _hydra_template_get_options_js() {
  if (!isset($_POST['template'])) {
    // Invalid form_build_id.
    drupal_json(array('data' => ''));
    exit;
  }
  $template_site = hydra_site_load($_POST['template']);

  // Retrieve the cached form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  if (!$form) {
    // Invalid form_build_id.
    drupal_json(array('data' => ''));
    exit;
  }

  $form['tables']['modules'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="template-tables">',
    '#suffix' => '</div>',
    _hydra_template_get_options_form($template_site),
  );
  form_set_cache($form_build_id, $form, $form_state);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );
  // Rebuild the form.
  $form = form_builder('hydra_template_form', $form, $form_state);
  // Render the new output.
  $output = theme('status_messages') . drupal_render($form['tables']['modules']);
  $GLOBALS['devel_shutdown'] =  FALSE;
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * Retrieve template site's tables options.
 */
function _hydra_get_template_tables() {
  $tables = array();
  $result = db_query("SELECT * FROM {hydra_template_tables}");
  while ($table = db_fetch_object($result)) {
    $tables[$table->tablename] = $table->copy;
  }
  return $tables;
}

/**
 * Create or copy tables according to the given configuration.
 */
function _hydra_install_tables($values) {
  $template_site = hydra_get_template_site();
  $template_tables = _hydra_get_template_tables();
  foreach ($template_tables as $table => $copy) {
    // Drop already installed tables (kill output)
    @db_query("DROP TABLE {$values['name']}_{$table}");
    if ($copy) {
      if (!db_query("CREATE TABLE {$values['name']}_{$table} LIKE {$template_site->name}_{$table}")) {
        drupal_set_message("Cannot create {$values['name']}_{$table}", 'error');
        return FALSE;
      }
      elseif (!db_query("INSERT INTO {$values['name']}_{$table} SELECT * FROM {$template_site->name}_{$table}")) {
        drupal_set_message("Cannot copy values from {$template_site->name}_{$table}", 'error');
        return FALSE;
      }
    }
    else {
      if (!db_query("CREATE TABLE {$values['name']}_{$table} LIKE {$template_site->name}_{$table}")) {
        drupal_set_message("Cannot create {$values['name']}_{$table}", 'error');
        return FALSE;
      }
    }
  }
  return TRUE;
}

/**
 * Generate the necessary filesystem setup while creating a new site.
 */
function _hydra_setup_dir($values) {
  global $base_path;
  $template_site = hydra_get_template_site();

  $ops = <<<EOSH
  cd sites;
  mkdir {$values['name']};
  chmod 775 {$values['name']};
  ln -s {$values['name']} {$values['hostname']};
  cd {$values['name']};
  mkdir files;
  chmod 777 files;
EOSH;
  exec($ops);

  $default_settings = './' . drupal_get_path('module', 'hydra') . '/misc/settings.php.template';
  $settings_file = "./sites/{$values['name']}/settings.php";
  $settings = array();
  $settings["db_prefix['default']"] = array(
    'value' => "{$values['name']}_",
  );
  $settings["cookie_domain"] = array(
    'value' => "{$values['hostname']}",
  );
  _hydra_rewrite_settings($default_settings, $settings_file, $settings);
}

/**
 * Remove the site's directory and symlink
 */
function _hydra_delete_dir($site) {
  $ops = <<<EOSH
  cd sites;
  rm -rf {$site->hostname};
  rm -rf {$site->name};
EOSH;
  exec($ops);
}

/**
 * Setup the newly created site.
 */
function _hydra_setup_site($values, $sid) {
  $site = hydra_site_load($sid);
  hydra_remote_variable_set($site, 'site_mail', $values['email']);
  hydra_remote_variable_set($site, 'site_name', $values['fullname']);
  hydra_remote_variable_set($site, 'site_frontpage', 'hydra_home');
  hydra_remote_variable_set($site, 'clean_url', 1);
  hydra_remote_variable_set($site, 'site_offline', (int)!$site->online);
  hydra_remote_variable_set($site, 'install_time', time());

  // Insert admin (uid 1) as site user
  $admin = user_load(array('uid' => 1));
  hydra_user_join($site, $admin, 0);

  // Check if we need to setup site admin
  if ($values['admin']) {
    $admin = user_load(array('name' => $values['admin']));
    hydra_user_join($site, $admin, 0);
    db_query("INSERT INTO {$site->name}_users_roles (uid, rid) VALUES (%d, %d)", $admin->uid, HYDRA_ADMINISTRATOR_RID);
  }

  // Eventually share global vocabularies
  if ($vids = hydra_get_global_vocs()) {
    foreach ($vids as $vid) {
      hydra_voc_share($site, $vid, 0, 1);
    }
  }
}

/**
 * Load, modify and save a copy of template settings.php.
 */
function _hydra_rewrite_settings($default_settings, $settings_file, $settings = array()) {
  // Build list of setting names and insert the values into the global namespace.
  $keys = array();
  foreach ($settings as $setting => $data) {
    $GLOBALS[$setting] = $data['value'];
    $keys[] = $setting;
  }

  $buffer = NULL;
  $first = TRUE;
  if ($fp = fopen($default_settings, 'r')) {
    // Step line by line through settings.php.
    while (!feof($fp)) {
      $line = fgets($fp);
      if ($first && drupal_substr($line, 0, 5) != '<?php') {
        $buffer = "<?php\n\n";
      }
      $first = FALSE;
      // Check for constants.
      if (drupal_substr($line, 0, 7) == 'define(') {
        preg_match('/define\(\s*[\'"]([A-Z_-]+)[\'"]\s*,(.*?)\);/', $line, $variable);
        if (in_array($variable[1], $keys)) {
          $setting = $settings[$variable[1]];
          $buffer .= str_replace($variable[2], " '" . $setting['value'] . "'", $line);
          unset($settings[$variable[1]]);
          unset($settings[$variable[2]]);
        }
        else {
          $buffer .= $line;
        }
      }
      // Check for variables.
      elseif (drupal_substr($line, 0, 1) == '$') {
        preg_match('/\$([^ ]*) /', $line, $variable);
        if (in_array($variable[1], $keys)) {
          // Write new value to settings.php in the following format:
          // $'setting' = 'value'; // 'comment'
          $setting = $settings[$variable[1]];
          $buffer .= '$'. $variable[1] . " = '" . $setting['value'] . "';" . (!empty($setting['comment']) ? ' // ' . $setting['comment'] . "\n" : "\n");
          unset($settings[$variable[1]]);
        }
        else {
          $buffer .= $line;
        }
      }
      else {
        $buffer .= $line;
      }
    }
    fclose($fp);

    // Add required settings that were missing from settings.php.
    foreach ($settings as $setting => $data) {
      if ($data['required']) {
        $buffer .= "\$$setting = '" . $data['value'] . "';\n";
      }
    }

    $fp = fopen($settings_file, 'w');
    if ($fp && fwrite($fp, $buffer) === FALSE) {
      drupal_set_message(t('Failed to modify %settings, please verify the file permissions.', array('%settings' => $settings_file)), 'error');
    }
  }
  else {
    drupal_set_message(t('Failed to open %settings, please verify the file permissions.', array('%settings' => $default_settings)), 'error');
  }
}

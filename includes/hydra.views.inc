<?php

/**
 * @file
 * Views support for the Hydra environment.
 * @ingroup hydra
 */

function hydra_views_data() {
  $data['hydra_nodes']['table']['group'] = t('hydra');

  $data['hydra_nodes']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Hydra site relationship'),
    'help' => t("Keeps track of the sites a node is shared on"),
    'weight' => -10,
  );

  // Node ID field.
  $data['hydra_nodes']['sid'] = array(
    'title' => t('Site id'),
    'help' => t('A site sharing the node.'),
    // Because this is a foreign key to the {node} table. This allows us to
    // have, when the view is configured with this relationship, all the fields
    // for the related node available.
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Sharing node relationship'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );

  $data['hydra_nodes']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['hydra_nodes']['promote'] = array(
    'title' => t('hydra promote'),
    'help' => t('Promote status for the node in a given hydra site.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['hydra_nodes']['sticky'] = array(
    'title' => t('hydra sticky'),
    'help' => t('Sticky status for the node in a given hydra site.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
